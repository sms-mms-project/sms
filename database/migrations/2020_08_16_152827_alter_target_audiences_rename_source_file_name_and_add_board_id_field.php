<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTargetAudiencesRenameSourceFileNameAndAddBoardIdField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('target_audiences', function (Blueprint $table) {
            $table->renameColumn('source_file_name', 'source_file');
            $table->unsignedInteger('board_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('target_audiences', function (Blueprint $table) {
            $table->renameColumn('source_file', 'source_file_name');
            $table->dropColumn('board_id');
        });
    }
}
