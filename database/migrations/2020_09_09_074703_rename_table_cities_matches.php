<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTableCitiesMatches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities_matches', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0)->nullable();
            $table->renameColumn('our_city_id', 'model_id');
            $table->dropColumn('board_city_id');
            $table->string('data', 255)->default('')->nullable();
        });

        Schema::rename('cities_matches', 'locations_matching');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::rename('locations_matching', 'cities_matches');

        Schema::table('cities_matches', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->renameColumn('model_id', 'our_city_id');
            $table->unsignedBigInteger('board_city_id')->default(0)->nullable();
            $table->dropColumn('data');
        });
    }
}
