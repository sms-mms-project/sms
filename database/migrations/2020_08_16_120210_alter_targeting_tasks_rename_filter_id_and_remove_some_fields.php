<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTargetingTasksRenameFilterIdAndRemoveSomeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('targeting_tasks', function (Blueprint $table) {
            $table->renameColumn('filter_id', 'source_id');
            $table->renameColumn('source_file_name', 'source_file');
            $table->dropColumn('board_olx_filter_id');
            $table->dropColumn('board_otomoto_filter_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('targeting_tasks', function (Blueprint $table) {
            $table->renameColumn('source_id', 'filter_id');
            $table->renameColumn('source_file', 'source_file_name');
            $table->unsignedBigInteger('board_olx_filter_id');
            $table->unsignedBigInteger('board_otomoto_filter_id');
        });
    }
}
