<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBoardOlxFiltersAddOfferTypeOnlyPremiumParseFromDateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('board_olx_pl_filters', function (Blueprint $table) {
            $table->tinyInteger('offer_type')->default(0);
            $table->tinyInteger('only_premium')->default(0);
            $table->timestamp('parse_from_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('board_olx_pl_filters', function (Blueprint $table) {
            $table->dropColumn('offer_type');
            $table->dropColumn('only_premium');
            $table->dropColumn('parse_from_date');
        });
    }
}
