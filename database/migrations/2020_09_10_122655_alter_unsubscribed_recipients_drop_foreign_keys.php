<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUnsubscribedRecipientsDropForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unsubscribed_recipients', function (Blueprint $table) {
            $table->dropForeign('unsubscribed_recipients_city_id_foreign');
            $table->dropForeign('unsubscribed_recipients_advertising_campaign_task_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unsubscribed_recipients', function (Blueprint $table) {
            $table->foreign('advertising_campaign_task_id')->references('id')->on('advertising_campaign_tasks');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }
}
