<?php

use Illuminate\Database\Seeder,
    \App\Models\Targeting\BoardCategory,
    \App\Models\Location\Region,
    \App\Models\Location\City;

class TargetingTasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $parentIds = [];


        for ($j = 0; $j < config('seed.targetingTasksCount'); $j++) {
            $data = [
                'name' => $faker->name,
                'source_type' => rand(0, 1),
                'source_id' => rand(0, config('seed.targetingTasksCount')),
                'source_file' => '',
                'contacts_count' => rand(0, 100),
                'board_id' => rand(1, config('seed.boardsCount')),
                'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
            ];
            $parentIds[] = array( 'target_audience_id' => DB::table('target_audiences')->insertGetId($data), 'data' => $data );
        }

        foreach ( $parentIds as $p ) {
            $data = $p['data'];
            unset($data['name']);
            unset($data['contacts_count']);
            $data['pars_start_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $data['pars_finish_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $data['last_update_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $data['state'] = !$data['source_type'] ? 0 : rand(0, 8);
            $data['stop_comment'] = '';
            $data['type'] = rand(0, 1);
            $data['is_archive'] = rand(0, 1);
            $data['created_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $data['updated_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $data['target_audience_id'] = $p['target_audience_id'];

            DB::table('targeting_tasks')->insertGetId($data);

            $categories = BoardCategory::where('board_id', $data['board_id'])->where('parent_id', 0)->get()->pluck('id')->toArray();
            $randCat = $categories[rand(0, sizeof($categories)-1)];
            //echo '$randCat ' . $randCat. "\n";
            $subCategories = BoardCategory::where('parent_id',$randCat)->get()->pluck('id')->toArray();
            //echo 'sizeof ' . sizeof($subCategories) . "\n";

            $randSubCat = $subCategories[rand(0, sizeof($subCategories)-1)];

            $regions = Region::where('country_id', 1)->get()->pluck('id')->toArray();
            $randReg = $regions[rand(0, sizeof($regions)-1)];
            $cities = City::where('region_id', $randReg)->get()->pluck('id')->toArray();
            $randCity = $cities[rand(0, sizeof($cities)-1)];

            $newData = [];
            $newData['category_id'] = $randCat;
            $newData['subcategory_id'] = $randSubCat;
            $newData['region_id'] = $randReg;
            $newData['city_id'] = $randCity;
            $newData['keywords'] = $faker->name;
            $newData['price_from'] = rand(0, 1000);
            $newData['price_to'] = rand(1000, 10000);
            $newData['location_distance'] = rand(10, 10000);
            $newData['is_ready_to_bargain'] = rand(0,1);
            $newData['offer_type'] = rand(0, 2);
            $newData['only_premium'] = rand(0, 1);
            $newData['parse_from_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $newData['created_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $newData['updated_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();

            DB::table('board_olx_pl_filters')->insertGetId($newData);
        }

    }
}
