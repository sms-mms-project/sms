<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $cities = [];
        for ($i = 0; $i < config('seed.citiesCount'); $i++) {
            $cities[] = [
                'name' => $faker->city,
                'region_id' => rand(1, config('seed.regionsCount')),
                'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
            ];
        }
        DB::table('cities')->insert($cities);
    }
}
