<?php

use Illuminate\Database\Seeder,
    Illuminate\Support\Facades\DB,
    App\Models\Targeting\BoardCategory,
    App\Classes\Parsers\OlxPlParser;

class BoardsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $parentIds = [];
        for ($i = 1; $i <= config('seed.boardsCount'); $i++) {

            for ($j = 0; $j < config('seed.boardsCategoriesCount'); $j++) {
                $data = [
                    'name' => $faker->city,
                    'board_id' => $i,
                    'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                ];
                $parentIds[] = array( 'parent_id' => DB::table('boards_categories')->insertGetId($data), 'board_id' => $i );
            }
        }

        foreach ( $parentIds as $p ) {
            for ($j = 0; $j < config('seed.boardsSubCategoriesCount'); $j++) {
                $data = [
                    'name' => $faker->city,
                    'board_id' => $p['board_id'],
                    'parent_id' => $p['parent_id'],
                    'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                    'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
                ];
                DB::table('boards_categories')->insertGetId($data);
            }
        }

        $olx = new OlxPlParser(true);
        $olx->parseCategories();
    }
}
