<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * API Routes
 */

//Соответсвует урл /api/
Route::group( ['namespace' => 'Api', 'prefix' => 'api'],function () {

    //Используют аутентификацию
    // Соответсвует урл /api/settings
    Route::group(['middleware' => 'auth:api', 'namespace' => 'Settings', 'prefix' => 'settings'], function () {
        //Соответсвует урл /api/settings/ppc
        Route::get('ppc', function (){

        });
    });

    //Без аутентификации
    //Соответсвует урл /api/settings/
    Route::group(['namespace' => 'Settings', 'prefix' => 'settings'], function () {

        //Работа с проксями
        Route::group(['prefix' => 'proxies'], function () {

            //Получение списка проксей /api/settings/proxies/
            Route::get('/', 'ProxiesController@index' );

            //Активация деактивация прокси /api/settings/proxies/{id}/status/
            Route::patch('/{id}/status/', 'ProxiesController@setStatus' )->where('id', '[1-9][0-9]*');

            //Активация деактивация многих проксей /api/settings/proxies/status/
            Route::patch('/status/', 'ProxiesController@setMultiplyStatuses' );

            //Удаление прокси /api/settings/proxies/{id}/
            Route::delete('/{id}/', 'ProxiesController@destroy' )->where('id', '[1-9][0-9]*');

            //Удаление проксей /api/settings/proxies/
            Route::delete('/', 'ProxiesController@destroyMultiply' );

            //Добавление проксей /api/settings/proxies/
            Route::post('/', 'ProxiesController@store' );

            //Обновление прокси /api/settings/proxies/{id}/
            Route::put('/{id}/', 'ProxiesController@update' )->where('id', '[1-9][0-9]*');

        });


        Route::resource('thematics', 'ThematicsController' )
            ->except(['create', 'show', 'edit'])
            ->names('api.settings.thematics');

        Route::resource('domains', 'DomainsRedirectsController' )
            ->except(['create', 'show', 'edit'])
            ->names('api.settings.domains');

    });

    //Страны, регионы, города
    Route::group(['namespace' => 'Location','prefix' => 'location/'], function () {

        //Получение списка стран /api/location/countries/
        Route::get('/countries/', 'LocationController@countries' );

        //Получение списка регионов /api/location/regions/
        Route::get('/regions/', 'LocationController@regions' );

        //Получение списка городов /api/location/cities/
        Route::get('/cities/', 'LocationController@cities' );

    });

    //Таргетинг
    Route::group(['namespace' => 'Targeting','prefix' => 'targeting/'], function () {

        Route::group(['prefix' => 'boards/'], function () {

            //Получение списка досок обявлений /api/targeting/boards/
            Route::get('/', 'BoardsController@index' );

            //Получение списка категорий досок обявлений /api/targeting/boards/categories
            Route::get('/categories/', 'BoardsCategoriesController@index' );

        });

        //Работа с тасками таргетинга
        Route::group(['prefix' => 'tasks'], function () {

            //Получение списка заданий на парсинг /api/targeting/tasks/
            Route::get('/', 'TargetingTasksController@index' );

            //Получение задания на парсинг /api/targeting/tasks/{id}
            Route::get('/{id}', 'TargetingTasksController@getTaskById' )->where('id', '[1-9][0-9]*' );

            //Добавление задания на парсинг /api/targeting/tasks/
            Route::post('/', 'TargetingTasksController@store' );

            //Отправить в архив /api/targeting/tasks/send-to-archive/{id}
            Route::delete('/send-to-archive/{id}/', 'TargetingTasksController@sendToArchive' )->where('id', '[1-9][0-9]*');

            //Отправить в архив массово /api/targeting/tasks/send-to-archive
            Route::delete('/send-to-archive/', 'TargetingTasksController@sendToArchiveMultiply' );

            //Изменение состояния /api/targeting/tasks/state/{id}
            Route::patch('/state/{id}/', 'TargetingTasksController@setState' )->where('id', '[1-9][0-9]*');

            //Активация деактивация многих проксей /api/targeting/tasks/state/
            Route::patch('/state/', 'TargetingTasksController@setStateMultiply' );

            //Обновление прокси /api/targeting/tasks/{id}/
            Route::put('/{id}/', 'TargetingTasksController@update' )->where('id', '[1-9][0-9]*');

        });

    });


    //Работа с сообщениями
    Route::group(['namespace' => 'Messages','prefix' => 'sms-mms-messages/'], function () {

        //Получение списка sms сообщений /api/sms-mms-messages/sms/
        Route::get('/sms/', 'SmsMmsMessagesController@index' );

        //Получение списка mms сообщений /api/sms-mms-messages/mms/
        Route::get('/mms/', 'SmsMmsMessagesController@mms' );

        //Получение списка голосовых сообщений /api/sms-mms-messages/voice/
        Route::get('/voice/', 'SmsMmsMessagesController@voice' );

    });

});

Route::get('/js/i18n.js', 'CoreController@i18n')->name('assets.i18n');


//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{any}', 'SpaController@index')->name('spa')->where('any', '.*');;


Auth::routes();

