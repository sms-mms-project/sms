<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 12.09.2020
 * Time: 9:19
 */

require __DIR__.'/../vendor/autoload.php';

$app = require_once __DIR__.'/../bootstrap/app.php';

$kernel = $app->make(Illuminate\Contracts\Http\Kernel::class);

$response = $kernel->handle(
    $request = Illuminate\Http\Request::capture()
);

//Запуск сообствено процесса парсинга
\App\Classes\Demons\ParserDemon::run();
exit();
class Task extends Threaded
{
    private $value;

    public function __construct(int $i)
    {
        $this->value = $i;
    }

    public function run()
    {
        $s=0;

        for ($i=0; $i<10000; $i++)
        {
            $s++;
        }

        echo "Task: {$this->value}\n";
    }
}

# Создаем пул из 4 потоков
$pool = new Pool(4);

for ($i = 0; $i < 15000; ++$i)
{
    $pool->submit(new Task($i));
}

while ($pool->collect());

$pool->shutdown();