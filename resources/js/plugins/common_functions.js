import Lang from 'lang.js';
let commonFunctions = {};
commonFunctions.install = function (Vue, options) {

    // Используется для вывода дат
    Vue.prototype.$getDateTime = function (dbDateTime, with_time = false, withMonthText = false)
    {
        let monthNames = [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Ноябрь',
            'Декабрь',
        ];
        let date = new Date(Date.parse(dbDateTime));
        return date.getDate() + '-' + (withMonthText ? monthNames[(''+date.getMonth()).slice(-2)] : ('0'+date.getMonth()).slice(-2) ) + '-' + date.getFullYear() + ( with_time ? ' ' + date.getHours() + ':' + date.getMinutes() + ':' + ('0'+date.getSeconds()).slice(-2): '' );
        //return date.getDate() + '-' + ('0' + date.getMonth()).slice(-2) + '-' + date.getFullYear()
    }



    // Используется для эмуляции функции trans  как у laravel, работает также. Вторым параметром можено передавать параметры: {age:32,name:"Петя"}
    // К примеру есть файл ru/messages.php и есть переменная 'confirmation' => " :name потвердите что вам :age года" . Если вызвать в компонентах vue trans('messages.confirmation', {age:32,name:"Петя"} )
    // то результат будет таким: Петя потвердите что вам 32 года
    Vue.prototype.trans = function( string, args = null)
    {
        /*
        if ( !Vue.prototype.__lang ) {
            Vue.prototype.__lang = new Lang({
                messages : window.i18n,
                locale: this.$store.getters.getLocale,
                fallback: this.$store.getters.getFallbackLocale
            });
        }

        Vue.prototype.__lang.setLocale(this.$store.getters.getLocale);
        Vue.prototype.__lang.setFallback(this.$store.getters.getFallback);
        let value = this.__lang.get(string, args);
        //return value ? value : ('!TranslateNotFound!-' + string);
        */

        let value = _.get(window.i18n, this.$store.getters.getLocale + '.' + string);
        if ( args )
            _.eachRight(args, (paramVal, paramKey) => {
                value = _.replace(value, `:${paramKey}`, paramVal);
            });

        return value != string ? value : ('!TranslateNotFound!-' + string);
    };

};
export default commonFunctions;

