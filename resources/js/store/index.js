import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
    state: {
        locale : window.locale,
        locales: window.locales,
        fallback_locale: window.fallback_locale,
        all_constants: window.all_constants,
    },
    mutations : {
        setLocale(state, locale)
        {
            state.locale = locale;
        },

        setFallbackLocale(state, fallback_locale)
        {
            state.fallback_locale = fallback_locale;
        }
    },
    actions: {
        setLocale({ commit }, locale)
        {
            Vue.$cookies.set('locale', locale, '31d', '/');
            commit('setLocale', locale);
        },

        setFallbackLocale({ commit }, fallback_locale)
        {
            //Vue.$cookies.set('fallback_locale', fallback_locale, '31d');
            commit('setFallbackLocale', fallback_locale);
        }
    },
    getters : {
        getLocale(state)
        {
            return state.locale;
        },

        getLocales(state)
        {
            return state.locales;
        },

        getFallbackLocale(state)
        {
            return state.fallback_locale;
        },
        CONS(state)
        {
            return state.all_constants;
        },

    }
});
