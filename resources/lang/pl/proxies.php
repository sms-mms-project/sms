<?php

return [
    'add' => 'Dodaj proxy',
    'notCheckedYet' => 'Serwer proxy nie został jeszcze zweryfikowany.',
    'partlyText1' => 'Proxy został :status pomyślnie',
    'tableFields1' => [
        ['key' => 'checkbox_field', 'label' =>''],
        ['key' => 'id', 'label' =>'ID'],
        ['key' => 'type', 'label' =>'Typ'],
        ['key' => 'ip', 'label' =>'IP'],
        ['key' => 'port', 'label' =>'Port'],
        ['key' => 'login', 'label' =>'login'],
        ['key' => 'password', 'label' =>'password'],
        ['key' => 'status', 'label' =>'Aktywny'],
        ['key' => 'busy_by_task_id', 'label' =>'Zajęty przy zlecaniu'],
        ['key' => 'is_banned', 'label' =>'Zakazany'],
        ['key' => 'operations', 'label' =>'Operacje'],
    ],
    'proxyType' => 'Typ proxy',
    'addFrom' => 'Dodaj z: ',
    'valid' => [
        'selectFile' => 'Wybierz plik z rozszerzeniem txt',
        'enterProxy' => 'Wpisz proxy, każdy nowy serwer proxy w nowej linii',
        'enterProxy2' => 'Wypełnij listę proxy w odpowiednim formacie',
        'errorOnRow' => ', błąd w linii :row',
        'enterProxy3' => 'Wypełnij listę proxy',
        'enterIp' => 'Wprowadź adres IP serwera proxy',
        'enterNumber' => 'Wpisz liczbę z przedziału 1-65535',
        'enterPort' => 'Wprowadź port proxy',
        'enterLogin' => 'Wpisz login',
        'enterPassword' => 'Wprowadź hasło',
        'enterValidIp' => 'Wprowadź adres IP serwera proxy w prawidłowym formacie',
    ],
    'selectFile' => 'Wybierz plik',
    'browse' => 'Przegląd',
    'proxyList' => 'Lista proxy',
    'format' => 'Format:',
    'sample' => 'Przykład:',
    'radioOptions2' => [
        [ 'text' => 'plik', 'value' => 0 ],
        [ 'text' => 'lista poniżej', 'value' => 1 ]
    ],
    'edit' => 'Edytuj proxy',
    'ip' => 'Adres IP serwera proxy',
    'port' => 'Port proxy',
    'login' => 'Zaloguj sie',
    'password' => 'Hasło'
];
