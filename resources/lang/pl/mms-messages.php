<?php

return [
    'type' => 'Typ MMS',
    'radioOptions1' => [
        [ 'text' => 'Wszystko', 'value' =>  -1 ],
        [ 'text' => 'MMS reklamowy', 'value' =>  1 ],
        [ 'text' => 'Ton MMS 1', 'value' =>  2 ],
        [ 'text' => 'MMS wysyłany po odsłuchaniu wiadomości głosowej', 'value' =>  3 ]
    ],
    'mmsText' => 'Tekst MMS',
    'valid' => [
        'enterText1' => 'Wpisz tekst składający się z co najmniej 3 znaków',
        'enterText2' => 'Wpisz dowolny tekst',
        'enterNumber1' => 'Wpisz liczbę całkowitą większą od zera',

    ],
    'mmsId' => 'Identyfikator wiadomości MMS',
    'thematics' => 'Przedmiot',
    'fullText' => 'Pełny tekst wiadomości',
    'fields' => [
        [ 'key' => 'id', 'label' => 'ID'],
        [ 'key' => 'text', 'label' => 'Wiadomość tekstowa'],
        [ 'key' => 'media', 'label' => 'Głoska bezdźwięczna'],
        [ 'key' => 'thematics_name', 'label' => 'Przedmiot'],
        [ 'key' => 'advertising_campaign_name', 'label' => 'Firma reklamowa'],
        [ 'key' => 'sent_count', 'label' => 'Liczba wysłanych'],
        [ 'key' => 'clicks_count', 'label' => 'Liczba przejść'],
        [ 'key' => 'used_simcards_count', 'label' => 'Liczba używanych kart SIM'],
        [ 'key' => 'created_at', 'label' => 'Data utworzenia'],
        [ 'key' => 'user_name', 'label' => 'Właściciel'],
    ],
    'noImages' => 'Bez zdjęć'
];
