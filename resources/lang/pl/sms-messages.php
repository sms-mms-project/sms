<?php

return [
    'type' => 'Typ SMS',
    'radioOptions1' => [
        [ 'text' => 'Wszystko', 'value' => -1 ],
        [ 'text' => 'SMS promocyjny', 'value' => 0 ],
        [ 'text' => 'Dźwięk SMS 1', 'value' => 2 ],
        [ 'text' => 'SMS wysłany po odsłuchaniu wiadomości głosowej', 'value' => 3 ],
        [ 'text' => 'Automatyczna sekretarka SMS', 'value' => 6 ],
    ],
    'smsText' => 'Tekst SMS',
    'valid' => [
        'enterText1' => 'Wpisz tekst składający się z co najmniej 3 znaków',
        'enterText2' => 'Wpisz dowolny tekst',
        'enterNuber1' => 'Wpisz liczbę całkowitą większą od zera',

    ],
    'smsId' => 'ID wiadomości SMS',
    'thematics' => 'Przedmiot',
    'fullText' => 'Pełny tekst wiadomości',
    'fields' => [
        [ 'key' => 'id', 'label' => 'ID'],
        [ 'key' => 'text', 'label' => 'Wiadomość tekstowa'],
        [ 'key' => 'thematics_name', 'label' => 'Przedmiot'],
        [ 'key' => 'advertising_campaign_name', 'label' => 'Firma reklamowa'],
        [ 'key' => 'sent_count', 'label' => 'Liczba wysłanych'],
        [ 'key' => 'clicks_count', 'label' => 'Liczba przejść'],
        [ 'key' => 'used_simcards_count', 'label' => 'Liczba używanych kart SIM'],
        [ 'key' => 'created_at', 'label' => 'Data utworzenia'],
        [ 'key' => 'user_name', 'label' => 'Właściciel'],
    ]
];
