<?php

return [
    'headers' => [
        'main' => 'Menu główne',
        'settings' => 'Ustawienia'
    ],
    'main' => 'Dom',
    'target' => [
        'main' => 'Kierowanie',
        'tasks' => 'Lista zadań',
        'tasksArchive' => 'Lista zadań z archiwum',
        'targetAudiencesBase' => 'Baza odbiorców docelowych',
        'targetAudiencesBaseArchive' => 'Baza odbiorców docelowych z archiwum',
    ],
    'advertisingCampaign' => [
        'main' => 'Firmy reklamowe',
        'tasks' => 'Lista zadań',
        'tasksArchive' => 'Lista zadań z archiwum'
    ],
    'analytics' => 'Analityka',
    'dialogues' => 'Dialogi SMS i MMS',
    'incomingCalls' => 'Połączenia przychodzące',
    'messages' => [
        'main' => 'Baza wiadomości',
        'sms' => 'Wiadomości SMS',
        'mms' => 'Wiadomości MMS',
        'voice' => 'Wiadomości głosowe'
    ],
    'spamList' => 'Arkusz spamu',
    'favorites' => 'Ulubione',
    'thematics' => 'Przedmiot',
    'simBanks' => 'Simbanks',
    'simCards' => 'Karty SIM',
    'proxies' => 'Serwer proxy',
    'domens' => 'Domeny i przekierowania',
    'profiles' => 'Profile',
];
