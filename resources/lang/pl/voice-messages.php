<?php

return [
    'voiceId' => 'ID wiadomości głosowej',
    'valid' => [
        'enterNumber1' => 'Wpisz liczbę całkowitą większą od zera',
    ],

    'thematics' => 'Przedmiot',
    'fields' => [
        [ 'key' =>'id', 'label' => 'ID'],
        [ 'key' =>'media', 'label' => 'Wiadomość głosowa'],
        [ 'key' =>'file_name', 'label' => 'Nazwa pliku'],
        [ 'key' =>'thematics_name', 'label' => 'Przedmiot'],
        [ 'key' =>'advertising_campaign_name', 'label' => 'Firma reklamowa'],
        [ 'key' =>'sent_count', 'label' => 'Liczba wysłanych'],
        [ 'key' =>'used_simcards_count', 'label' => 'Liczba używanych kart SIM'],
        [ 'key' =>'created_at', 'label' => 'Data utworzenia'],
        [ 'key' =>'user_name', 'label' => 'Właściciel'],
    ],
    'noImages' => 'Bez zdjęć'
];
