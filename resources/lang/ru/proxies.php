<?php

return [
    'add' => 'Добавить прокси',
    'notCheckedYet' => 'Прокси пока не проверен.',
    'partlyText1' => 'Прокси успешно :status',
    'tableFields1' => [
        ['key' => 'checkbox_field', 'label' =>''],
        ['key' => 'id', 'label' =>'ID'],
        ['key' => 'type', 'label' =>'Тип'],
        ['key' => 'ip', 'label' =>'IP'],
        ['key' => 'port', 'label' =>'Порт'],
        ['key' => 'login', 'label' =>'login'],
        ['key' => 'password', 'label' =>'password'],
        ['key' => 'status', 'label' =>'Активный'],
        ['key' => 'busy_by_task_id', 'label' =>'Занят под задание'],
        ['key' => 'is_banned', 'label' =>'Забанен'],
        ['key' => 'operations', 'label' =>'Операции'],
    ],
    'proxyType' => 'Тип прокси',
    'addFrom' => 'Добавить из: ',
    'valid' => [
        'selectFile' => 'Выберите файл с расширением txt',
        'enterProxy' => 'Введите прокси, каждая новая прокси с новой строки',
        'enterProxy2' => 'Заполните список прокси в правильном формате',
        'errorOnRow' => ', ошибка в строке :row',
        'enterProxy3' => 'Заполните список прокси',
        'enterIp' => 'Введите IP адресс прокси',
        'enterNumber' => 'Введите число в интервале 1-65535',
        'enterPort' => 'Введите порт прокси',
        'enterLogin' => 'Введите логин',
        'enterPassword' => 'Введите пароль',
        'enterValidIp' => 'Введите IP адресс прокси в правильном формате',
    ],
    'selectFile' => 'Выберите файл',
    'browse' => 'Обзор',
    'proxyList' => 'Список прокси',
    'format' => 'Формат:',
    'sample' => 'Пример:',
    'radioOptions2' => [
        [ 'text' => 'файла', 'value' => 0 ],
        [ 'text' => 'списка ниже', 'value' => 1 ]
    ],
    'edit' => 'Редактировать прокси',
    'ip' => 'IP адресс прокси',
    'port' => 'Порт прокси',
    'login' => 'Логин',
    'password' => 'Пароль'
];
