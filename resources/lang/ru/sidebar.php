<?php

return [
    'headers' => [
        'main' => 'Основное меню',
        'settings' => 'Настройки'
    ],
    'main' => 'Главная',
    'target' => [
        'main' => 'Таргетинг',
        'tasks' => 'Список заданий',
        'tasksArchive' => 'Список заданий из архива',
        'targetAudiencesBase' => 'База целевых аудиторий',
        'targetAudiencesBaseArchive' => 'База целевых аудиторий из архива'
    ],
    'advertisingCampaign' => [
        'main' => 'Рекламные компании',
        'tasks' => 'Список заданий',
        'tasksArchive' => 'Список заданий из архива'
    ],
    'analytics' => 'Аналитика',
    'dialogues' => 'Диалоги SMS и MMS',
    'incomingCalls' => 'Входящие вызовы',
    'messages' => [
        'main' => 'База сообщений',
        'sms' => 'SMS сообщения',
        'mms' => 'MMS сообщения',
        'voice' => 'Голосовые сообщения'
    ],
    'spamList' => 'Спам лист',
    'favorites' => 'Избранное',
    'thematics' => 'Тематика',
    'simBanks' => 'Симбанки',
    'simCards' => 'Симкарты',
    'proxies' => 'Прокси сервера',
    'domens' => 'Домены и редиректы',
    'profiles' => 'Профили',
];
