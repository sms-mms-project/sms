<?php

return [
    'type' => 'Тип MMS',
    'radioOptions1' => [
        [ 'text' => 'Все', 'value' =>  -1 ],
        [ 'text' => 'Рекламные MMS', 'value' =>  1 ],
        [ 'text' => 'MMS тон 1', 'value' =>  2 ],
        [ 'text' => 'MMS отправляемые после прослушивания голосового сообщения', 'value' =>  3 ]
    ],
    'mmsText' => 'Текст MMS сообщения',
    'valid' => [
        'enterText1' => 'Введите текст не менее 3х символов',
        'enterText2' => 'Введите любой текст',
        'enterNumber1' => 'Введите целое число больше нуля',

    ],
    'mmsId' => 'ID MMS сообщения',
    'thematics' => 'Тематика',
    'fullText' => 'Полный текст сообщения',
    'fields' => [
        [ 'key' => 'id', 'label' => 'ID'],
        [ 'key' => 'text', 'label' => 'Текст сообщения'],
        [ 'key' => 'media', 'label' => 'Медиа'],
        [ 'key' => 'thematics_name', 'label' => 'Тематика'],
        [ 'key' => 'advertising_campaign_name', 'label' => 'Рекламная компания'],
        [ 'key' => 'sent_count', 'label' => 'Кол. отправленных'],
        [ 'key' => 'clicks_count', 'label' => 'Кол. переходов'],
        [ 'key' => 'used_simcards_count', 'label' => 'Кол. исп. симкарт'],
        [ 'key' => 'created_at', 'label' => 'Дата создания'],
        [ 'key' => 'user_name', 'label' => 'Владелец'],
    ],
    'noImages' => 'Нет картинок'
];
