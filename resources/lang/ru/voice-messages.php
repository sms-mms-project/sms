<?php

return [
    'voiceId' => 'ID голосового сообщения',
    'valid' => [
        'enterNumber1' => 'Введите целое число больше нуля',
    ],

    'thematics' => 'Тематика',
    'fields' => [
        [ 'key' =>'id', 'label' => 'ID'],
        [ 'key' =>'media', 'label' => 'Голосовое сообщение'],
        [ 'key' =>'file_name', 'label' => 'Название файла'],
        [ 'key' =>'thematics_name', 'label' => 'Тематика'],
        [ 'key' =>'advertising_campaign_name', 'label' => 'Рекламная компания'],
        [ 'key' =>'sent_count', 'label' => 'Кол. отправленных'],
        [ 'key' =>'used_simcards_count', 'label' => 'Кол. исп. симкарт'],
        [ 'key' =>'created_at', 'label' => 'Дата создания'],
        [ 'key' =>'user_name', 'label' => 'Владелец'],
    ],
    'noImages' => 'Нет картинок'
];
