<?php

return [
    'type' => 'Тип SMS',
    'radioOptions1' => [
        [ 'text' => 'Все', 'value' => -1 ],
        [ 'text' => 'Рекламные SMS', 'value' => 0 ],
        [ 'text' => 'SMS тон 1', 'value' => 2 ],
        [ 'text' => 'SMS отправленные после прослушивания голосового сообщения', 'value' => 3 ],
        [ 'text' => 'SMS автоответчик', 'value' => 6 ],
    ],
    'smsText' => 'Текст SMS сообщения',
    'valid' => [
        'enterText1' => 'Введите текст не менее 3х символов',
        'enterText2' => 'Введите любой текст',
        'enterNuber1' => 'Введите целое число больше нуля',

    ],
    'smsId' => 'ID SMS сообщения',
    'thematics' => 'Тематика',
    'fullText' => 'Полный текст сообщения',
    'fields' => [
        [ 'key' => 'id', 'label' => 'ID'],
        [ 'key' => 'text', 'label' => 'Текст сообщения'],
        [ 'key' => 'thematics_name', 'label' => 'Тематика'],
        [ 'key' => 'advertising_campaign_name', 'label' => 'Рекламная компания'],
        [ 'key' => 'sent_count', 'label' => 'Кол. отправленных'],
        [ 'key' => 'clicks_count', 'label' => 'Кол. переходов'],
        [ 'key' => 'used_simcards_count', 'label' => 'Кол. исп. симкарт'],
        [ 'key' => 'created_at', 'label' => 'Дата создания'],
        [ 'key' => 'user_name', 'label' => 'Владелец'],
    ]
];
