<?php

namespace App\Console\Commands;

use App\Classes\Parsers\OlxPlParser;
use App\Models\Targeting\BoardOlxFilter;
use Illuminate\Console\Command;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:parsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //$olx = new OlxPlParser();
        $filter = BoardOlxFilter::find(101)->toArray();
        dump($filter);
        echo OlxPlParser::getUrl($filter)['url'] . "\n";
        return 0;
    }
}
