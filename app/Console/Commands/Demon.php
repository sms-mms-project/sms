<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Settings\Proxies\Proxies;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class Demon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'demon:start {user?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $server = new \Swoole\Server("192.168.0.200", 9501);
        $server->on('connect', function ($server, $fd){
            echo "New connection established: #{$fd}.\n";
        });
        $server->on('receive', function ($server, $fd, $from_id, $data) {

            $data2 = json_encode(DB::table('proxies')->get());
            $server->send($fd, "Echo to #{$fd}: \n".$data. "\n" . $data2);
            $server->close($fd);

        });
        $server->on('close', function ($server, $fd) {
            echo "Connection closed: #{$fd}.\n";
        });

        $this->info('Server started YESS!');

        \Swoole\Timer::tick(1000, function () {
            echo "hello\n";
        });
        //\Swoole\Event::wait();
        register_shutdown_function(function () {
            \Swoole\Event::wait();
        });

        //$server->start();
        return 0;
    }
}
