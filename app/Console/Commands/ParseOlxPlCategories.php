<?php

namespace App\Console\Commands;

use App\Classes\Parsers\OlxPlParser;
use Illuminate\Console\Command;

class ParseOlxPlCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:olxplx-categories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run olx.pl categories parser';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $olx = new OlxPlParser();
        $olx->parseCategories();
        return 0;
    }
}
