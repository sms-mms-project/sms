<?php

namespace App\Http\Controllers;

use App\Models\Targeting\TargetingTask;
use Illuminate\Http\Request;

class CoreController extends Controller
{

    public function i18n()
    {
        \Debugbar::disable();
        $locales = config('app.locales');
        $i18n = array();
        foreach ( $locales as $locale => $title ) {
            $files = glob(resource_path('lang/'.$locale.'/*.php'));
            $i18n[$locale] = array();
            if ($files) {
                foreach ( $files as $file ) {
                    $name = basename($file, '.php');
                    $i18n[$locale][$name] = require $file;
                }
            }
        }
        $ret = '';
        $ret .= ('window.locale = "' . app()->getLocale() . '";' . "\r\n");
        $ret .= ('window.fallback_locale = "' . config('app.fallback_locale') . '";' . "\r\n");
        $ret .= ('window.locales = ' . json_encode( config('app.locales' ) ) . ';' . "\r\n");
        $ret .= ('window.i18n = ' . json_encode($i18n) . ';' . "\r\n");
        $ret .= ('window.all_constants = ' . json_encode($this->constants()) . ';' . "\r\n");
        return response()->make($ret, 200, ['Content-type: text/javascript']);
    }

    public function constants( )
    {
        return array(
            //Таргетинг
            'STATE_TASK_NOT_WORKING' => TargetingTask::STATE_TASK_NOT_WORKING,
            'STATE_TASK_STARTING' => TargetingTask::STATE_TASK_STARTING,
            'STATE_TASK_WORKING' => TargetingTask::STATE_TASK_WORKING,
            'STATE_TASK_STOPPING' => TargetingTask::STATE_TASK_STOPPING,
            'STATE_TASK_FREEZING' => TargetingTask::STATE_TASK_FREEZING,
            'STATE_TASK_FREEZED' => TargetingTask::STATE_TASK_FREEZED,
            'STATE_TASK_UNFREEZE' => TargetingTask::STATE_TASK_UNFREEZE,
            'STATE_TASK_STOPPING_BY_ADMIN' => TargetingTask::STATE_TASK_STOPPING_BY_ADMIN,
            'STATE_TASK_STOPPED_BY_ADMIN' => TargetingTask::STATE_TASK_STOPPED_BY_ADMIN,
            'TASK_SOURCE_TYPE_FILE' => TargetingTask::TASK_SOURCE_TYPE_FILE,
            'TASK_SOURCE_TYPE_BOARD' => TargetingTask::TASK_SOURCE_TYPE_BOARD,
        );
    }
}
