<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 22.07.2020
 * Time: 13:16
 */

namespace App\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Database\Eloquent\Model,
    App\Models\Targeting\Board,
    Illuminate\Support\Facades\DB;

class BoardsController extends Controller
{
    /**
     * Repository instance
     * @var Model $model
     */
    public $model;


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index( Request $request)
    {

        $onlyActive = $request->has('onlyActive') ? intval($request->get('onlyActive') ) : true;

        $data = Board::when($onlyActive, function ($query) {
            return $query->where( 'status', '=', 1 );
        })
            ->select('id', 'name', 'uid_name', 'status', 'checking_state')
            ->get();


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $data->count()],
            'items' => $data->toArray(),
        );

        return response()->json($return);
    }
}