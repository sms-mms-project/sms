<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 22.07.2020
 * Time: 13:16
 */

namespace App\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Database\Eloquent\Model,
    App\Models\Targeting\BoardCategory,
    Illuminate\Support\Facades\DB;

class BoardsCategoriesController extends Controller
{
    /**
     * Repository instance
     * @var Model $model
     */
    public $model;


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index( Request $request)
    {

        $boardId = $request->has('boardId') ? intval($request->get('boardId') ) : false;

        $data = BoardCategory::when($boardId, function ($query) use($boardId) {
            return $query->where( 'board_id', '=', $boardId );
        })
            ->select('id', 'name', 'board_id', 'parent_id')
            ->get();


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $data->count()],
            'items' => $data->toArray(),
        );

        return response()->json($return);
    }
}