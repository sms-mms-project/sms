<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 22.07.2020
 * Time: 13:16
 */

namespace App\Http\Controllers\Api\Targeting;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Database\Eloquent\Model,
    App\Models\Targeting\TargetingTask,
    Validator,
    App\Models\Targeting\Board,
    App\Models\Targeting\TargetAudience,
    App\Models\Targeting\TargetContact,
    Illuminate\Support\Facades\DB,
    App\Models\Targeting\BoardOlxFilter,
    App\Models\Targeting\BoardOtomotoFilter;



class TargetingTasksController extends Controller
{
    /**
     * Repository instance
     * @var Model $model
     */
    public $model;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index( Request $request)
    {
        $itemsPerPage = $request->has('itemsPerPage') ? $request->get('itemsPerPage') : 25;
        $page = $request->has('page') ? $request->get('page') : 1;
        $filter = $request->has('filter') ? json_decode($request->get('filter'),true) : null;

        $keyword = $filter && isset($filter['keyword']) ? $filter['keyword'] : false;

        $contactsFrom = $filter && isset($filter['contacts']) && isset($filter['contacts']['from']) && !empty($filter['contacts']['from']) ? intval($filter['contacts']['from']) : false;
        $contactsTo = $filter && isset($filter['contacts']) && isset($filter['contacts']['to']) && !empty($filter['contacts']['to']) ? intval($filter['contacts']['to']) : false;

        $locationRegion = $filter && isset($filter['location']) && isset($filter['location']['region']) && !empty($filter['location']['region']) && (intval($filter['location']['region']) > 0) ? intval($filter['location']['region']) : false;
        $locationCity = $filter && isset($filter['location']) && isset($filter['location']['city']) && !empty($filter['location']['city']) && (intval($filter['location']['city']) > 0) ? intval($filter['location']['city']) : false;

        $boardId = $filter && isset($filter['board']) && isset($filter['board']['id']) && !empty($filter['board']['id']) && (intval($filter['board']['id']) > 0) ? intval($filter['board']['id']) : false;
        $categoryId = $filter && isset($filter['board']) && isset($filter['board']['categoryId']) && !empty($filter['board']['categoryId']) && (intval($filter['board']['categoryId']) > 0) ? intval($filter['board']['categoryId']) : false;
        $subCategoryId = $filter && isset($filter['board']) && isset($filter['board']['subCategoryId']) && !empty($filter['board']['subCategoryId']) && (intval($filter['board']['subCategoryId']) > 0) ? intval($filter['board']['subCategoryId']) : false;

        $targetingTasks = TargetingTask::addPagination($itemsPerPage, $page);

        $tableName = (new TargetingTask)->getTable();

        $data = $targetingTasks
            ->where('is_archive', '=', 0)
            ->with('board','targetAudience')
            ->orderBy($tableName . '.id', 'desc')
            ->get();

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $targetingTasks->count()],
            'items' => $data->toArray()
        );

        return response()->json($return);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $jsonData = array();
        $jsonData['task'] = json_decode( $request->all()['task'], true);
        if (isset($request->file()['source_file']))
            $jsonData['source_file'] = $request->file('source_file');

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';

        $rules = $this->boardsValidationRules($jsonData['task']['source_type'], $jsonData['task']['board_id']);
        $validator = Validator::make($jsonData, $rules);

        if ($validator->fails()) {
            $return['errorCode'] = 1;
            $return['message'] = array_values((array)($validator->messages()->all()));
            return response()->json($return);
        }
        //Если есть файл с контактами то добавляем их и создаем БЦА и задание на парсинг файла
        if ($jsonData['task']['source_type'] == 0) {
            $this->addContactsFromFile($jsonData, $request);
        } else
            //Иначе создаем задание на парсинг из доски обявлений
            $this->addTargetTaskFromBoard($jsonData, $request);

        return response()->json($return);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $jsonData = array();
        $jsonData['task'] = json_decode( $request->all()['task'], true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';

        $task = TargetingTask::find($id);

        if (!$task) {
            $return['errorCode'] = 1;
            $return['message'] = trans('common.objectNotFound');
        }

        if ( !$return['errorCode'] && ($task->board_id != $jsonData['task']['board_id'] && $task->targetAudience->contacts_count > 0 ) ) {
            $return['errorCode'] = 3;
            $return['message'] = trans('target.notAllowedChangeBoard');
        }

        if (!$return['errorCode']) {

            $rules = $this->boardsValidationRules($jsonData['task']['source_type'], $jsonData['task']['board_id']);
            $validator = Validator::make($jsonData, $rules);

            if ($validator->fails()) {
                $return['errorCode'] = 1;
                $return['message'] = array_values((array)($validator->messages()->all()));
                return response()->json($return);
            }

            //Если источник файл то тогда мы можем изменить только название БЦА
            if (!$task->source_type ) {
                $targetAudience = $task->targetAudience;
                $targetAudience->name = $jsonData['task']['name'];
                $targetAudience->save();
            } else
                $this->updateTargetTaskFromBoard($task, $jsonData, $request);

            $return['json'] = $jsonData['task'];
        }

        return response()->json($return);
    }


    /**
     * Добавляет контакты из файла
     *
     * @param  array $jsonData
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addContactsFromFile($jsonData, Request $request)
    {
        $file = $request->file('source_file');
        $data = file($file->getRealPath());
        $keys = array('phone_str', 'name');
        $contacts = array();
        $cx = 0;
        foreach ($data as $line) {
            $contacts[$cx] = array_combine($keys, str_getcsv($line, ';'));
            $contacts[$cx]['phone'] = preg_replace('`[^\d]+`si', '', $contacts[$cx]['phone_str'] );
            $cx++;
        }

        //Создаем БЦА
        $targetAudienceData = [
            'name' => $jsonData['task']['name'],
            'source_type' => $jsonData['task']['source_type'],
            'source_id' => 0,
            'source_file' => '',
            'contacts_count' => sizeof($contacts),
            'board_id' => $jsonData['task']['board_id'],
            'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
            'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString()
        ];

        $targetAudience = TargetAudience::create($targetAudienceData);

        //Создаем задание на таргетинг
        unset($targetAudienceData['name']);
        unset($targetAudienceData['contacts_count']);
        $targetingTaskData = $targetAudienceData;
        $targetingTaskData['pars_start_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        $targetingTaskData['pars_finish_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        $targetingTaskData['last_update_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        $targetingTaskData['state'] = TargetingTask::STATE_TASK_NOT_WORKING;
        $targetingTaskData['stop_comment'] = '';
        $targetingTaskData['type'] = 0;
        $targetingTaskData['is_archive'] = 0;
        $targetingTaskData['target_audience_id'] = $targetAudience->id;

        $targetingTask = TargetingTask::create($targetingTaskData);

        $fileName = $targetingTask->id . '/' . $file->getClientOriginalName();

        \Storage::disk('contacts')->put($fileName, file_get_contents($request->file('source_file')));

        $fileUrl = \Storage::disk('contacts')->url($fileName);

        $targetAudience->source_file = $fileUrl;
        $targetAudience->save();

        $targetingTask->source_file = $fileUrl;
        $targetingTask->save();

        //Добавление контактов
        foreach ($contacts as $key => $val) {
            $contacts[$key]['target_audience_id'] = $targetAudience->id;
            $contacts[$key]['country_id'] = 0;
            $contacts[$key]['region_id'] = 0;
            $contacts[$key]['city_id'] = 0;
            $contacts[$key]['board_id'] = 0;
            $contacts[$key]['created_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
            $contacts[$key]['updated_at'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        }

        $tableName = (new TargetContact)->getTable();
        DB::table($tableName)->insert($contacts);

        //dump($targetAudience->id, $targetingTask->id, $fileUrl, $contacts);
    }

    /**
     * Добавляет задание 
     *
     * @param  array $jsonData
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addTargetTaskFromBoard($jsonData, Request $request)
    {

        //Создаем БЦА
        $targetAudienceData = [
            'name' => $jsonData['task']['name'],
            'source_type' => intval($jsonData['task']['source_type']),
            'source_id' => 0,
            'source_file' => '',
            'contacts_count' => 0,
            'board_id' => intval($jsonData['task']['board_id']),
            'created_at' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
            'updated_at' => \Illuminate\Support\Carbon::now()->toDateTimeString()
        ];

        $targetAudience = TargetAudience::create($targetAudienceData);

        //Создаем задание на таргетинг
        unset($targetAudienceData['name']);
        unset($targetAudienceData['contacts_count']);
        $targetingTaskData = $targetAudienceData;
        //$targetingTaskData['pars_start_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        //$targetingTaskData['pars_finish_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        //$targetingTaskData['last_update_date'] = \Illuminate\Support\Carbon::now()->toDateTimeString();
        $targetingTaskData['state'] = TargetingTask::STATE_TASK_NOT_WORKING;
        $targetingTaskData['stop_comment'] = '';
        $targetingTaskData['type'] = intval($jsonData['task']['type']);
        $targetingTaskData['is_archive'] = 0;
        $targetingTaskData['target_audience_id'] = $targetAudience->id;

        $targetingTask = TargetingTask::create($targetingTaskData);

        //Добавляем фильтр в таблицу фильтров
        $board = Board::find($jsonData['task']['board_id']);

        //Готовим данные для внесения в таблицу
        $jsonData['task']['boardFilter'] = $this->boardsPrepareInput($jsonData['task']['boardFilter'], $jsonData['task']['board_id']);

        $tableName = $board['filter_table_name'];
        $source_id = DB::table($tableName)->insertGetId($jsonData['task']['boardFilter']);

        //Обновляем связи
        $targetAudience->source_id = $source_id;
        $targetAudience->save();

        $targetingTask->source_id = $source_id;
        $targetingTask->save();

    }


    /**
     * Обновление задания
     *
     * @param  object $task
     * @param  array $jsonData
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateTargetTaskFromBoard($task, $jsonData, Request $request)
    {

        //Обновляем БЦА
        $targetAudience = $task->targetAudience;
        $targetAudience->name = $jsonData['task']['name'];
        $targetAudience->updated_at = \Illuminate\Support\Carbon::now()->toDateTimeString();
        $targetAudience->save();


        //Изменение доски обявлений возможна только в том случай если по данному таску нет ни каких контактов и только в этом случай создаётся новый фильтр а старый удаляется
        if ( $task->board_id != $jsonData['task']['board_id'] ) {
            if ( !$targetAudience->contacts_count || $targetAudience->contacts_count == 0 ) {

                // Удаляем старый фильтр
                DB::table($task->board->filter_table_name)->where('id', '=', $task->source_id)->delete();

                // Готовим новый фильтр
                $jsonData['task']['boardFilter'] = $this->boardsPrepareInput($jsonData['task']['boardFilter'], $jsonData['task']['board_id']);

                // Берем новую доску
                $board = Board::find($jsonData['task']['board_id']);

                // Создаём новый фильтр
                $source_id = DB::table($board['filter_table_name'])->insertGetId($jsonData['task']['boardFilter']);

                // Обновляем связи БЦА
                $targetAudience->source_id = $source_id;
                $targetAudience->board_id  = $jsonData['task']['board_id'];
                $targetAudience->save();

                // Обновляем связи в таске
                $task->source_id = $source_id;
                $task->board_id  = $jsonData['task']['board_id'];
                $task->type = $jsonData['task']['type'];
                $task->save();
            } else {

                // Готовим фильтр
                $jsonData['task']['boardFilter'] = $this->boardsPrepareInput($jsonData['task']['boardFilter'], $task->board_id);

                // Обновляем фильтр
                $source_id = DB::table($task->board->filter_table_name)->where('id', '=',$task->source_id )->update($jsonData['task']['boardFilter']);

                // Обновляем таск
                $task->type = $jsonData['task']['type'];
                $task->save();
            }
        } else {
            // Готовим фильтр
            $jsonData['task']['boardFilter'] = $this->boardsPrepareInput($jsonData['task']['boardFilter'], $task->board_id);

            // Обновляем фильтр
            $source_id = DB::table($task->board->filter_table_name)->where('id', '=',$task->source_id )->update($jsonData['task']['boardFilter']);

            // Обновляем таск
            $task->type = $jsonData['task']['type'];
            $task->save();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTaskById($id)
    {
        $targetingTask = TargetingTask::with('board','targetAudience')->find( $id );


        if ( !$targetingTask->source_type )
            $boardFilter = null;
        else
            $boardFilter = $this->getBoardFilter($targetingTask->board->uid_name, $targetingTask->board->filter_table_name, $targetingTask->source_id);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(

        );

        if (!$targetingTask) {
            $return['errorCode'] = 1;
            $return['message'] = trans('common.objectNotFound');
        } else {
            $data = array();
            $data['name'] = $targetingTask->targetAudience->name;
            $data['source_type'] = $targetingTask->source_type;
            $data['source_file'] = $targetingTask->source_file;
            $data['type'] = $targetingTask->type;
            $data['board_id'] = $targetingTask->board_id;
            $data['boardFilter'] = $boardFilter;
            $return['data'] = $data;
        }

        return response()->json($return);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $proxy  = Proxies::find($id);

        $jsonData = json_decode( $request->getContent(), true);


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
        );

        if (!$proxy) {
            $return['errorCode'] = 1;
            $return['message'] = 'Обект не найден';
        }

        if (!$jsonData || !isset($jsonData['value']) ) {
            $return['errorCode'] = 2;
            $return['message'] = 'Не пераданны необходимые данные';
        }

        if (!$return['errorCode'])
            $proxy->delete();

        return response()->json($return);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroyMultiply(Request $request)
    {
        $jsonData = json_decode( $request->getContent(), true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(

        );

        if (!$jsonData || !isset($jsonData['value']) || !isset($jsonData['ids']) || !sizeof($jsonData['ids']) ) {
            $return['errorCode'] = 2;
            $return['message'] = 'Не пераданны необходимые данные';
        }

        if (!$return['errorCode'])
            Proxies::destroy($jsonData['ids']);


        return response()->json($return);

    }

    /**
     * Возвращяет правила для валидации в зависимости от типа или выбранной доски обявлений
     *
     * @param int $sourceType
     * @param int $boardId
     * @return array
     */
    public function boardsValidationRules($sourceType, $boardId)
    {
        $rules = [
            'task.name' => 'required|min:3',
            'task.source_type' => 'required|digits_between:0,1',
        ];

        if (!$sourceType)
            $rules['source_file'] = 'file|mimes:csv,txt';
        else {
            $board = Board::find($boardId);

            $rules['task.type'] = 'required|digits_between:0,1';
            $rules['task.board_id'] = 'required|integer|min:1';

            switch ($board['uid_name']) {
                case 'olx_pl' : {
                    $rules['task.boardFilter.category_id'] = 'integer|min:0';
                    $rules['task.boardFilter.subcategory_id'] = 'integer|min:0';
                    $rules['task.boardFilter.region_id'] = 'integer|min:0';
                    $rules['task.boardFilter.city_id'] = 'integer|min:0';
                    $rules['task.boardFilter.location_distance'] = 'present|integer|min:1';
                    $rules['task.boardFilter.keywords'] = 'string:min:3';
                    $rules['task.boardFilter.price_from'] = 'integer|min:0';
                    $rules['task.boardFilter.price_to'] = 'integer|min:1';
                    $rules['task.boardFilter.offer_type'] = 'integer|between:0,2';
                    $rules['task.boardFilter.is_ready_to_bargain'] = 'integer|between:0,1';
                    $rules['task.boardFilter.only_premium'] = 'integer|between:0,1';
                    $rules['task.boardFilter.parse_from_date'] = 'date';
                } break;

                case 'otomoto_pl' : {

                } break;
            }
        }

        return $rules;
    }

    /**
     * Возвращяет приводит данные в нормальный вид
     *
     * @param array $boardFilter
     * @param int $boardId
     * @return array
     */
    public function boardsPrepareInput($boardFilter, $boardId)
    {
        $board = Board::find($boardId);

        switch ($board['uid_name']) {
            case 'olx_pl' : {
                $boardFilter['category_id'] = intval($boardFilter['category_id']);
                $boardFilter['subcategory_id'] = intval($boardFilter['subcategory_id']);
                $boardFilter['region_id'] = intval($boardFilter['region_id']);
                $boardFilter['city_id'] = intval($boardFilter['city_id']);
                $boardFilter['location_distance'] = intval($boardFilter['location_distance']);
                $boardFilter['price_from'] = intval($boardFilter['price_from']);
                $boardFilter['price_to'] = intval($boardFilter['price_to']);
                $boardFilter['offer_type'] = intval($boardFilter['offer_type']);
                $boardFilter['is_ready_to_bargain'] = intval($boardFilter['is_ready_to_bargain']);
                $boardFilter['only_premium'] = intval($boardFilter['only_premium']);
                $boardFilter['parse_from_date'] = $boardFilter['parse_from_date'] ? $boardFilter['parse_from_date'] : null;

            } break;

            case 'otomoto_pl' : {

            } break;
        }

        return $boardFilter;
    }

    /**
     * Возвращяет фильтр
     *
     * @param string $boardUidName
     * @param string $tableName
     * @param int $source_id
     * @return array
     */
    public function getBoardFilter($boardUidName, $tableName, $source_id)
    {
        $filter = DB::table($tableName)->find($source_id);
        $boardFilter = [];

        switch ($boardUidName) {
            case 'olx_pl' : {
                $boardFilter['category_id'] = intval($filter->category_id);
                $boardFilter['subcategory_id'] = intval($filter->subcategory_id);
                $boardFilter['region_id'] = intval($filter->region_id);
                $boardFilter['city_id'] = intval($filter->city_id);
                $boardFilter['location_distance'] = !$filter->location_distance ? '' : $filter->location_distance;
                $boardFilter['keywords'] = $filter->keywords;
                $boardFilter['price_from'] = !$filter->price_from ? '': $filter->price_from;
                $boardFilter['price_to'] = !$filter->price_to ? '' : $filter->price_to;
                $boardFilter['offer_type'] = intval($filter->offer_type);
                $boardFilter['is_ready_to_bargain'] = intval($filter->is_ready_to_bargain);
                $boardFilter['only_premium'] = intval($filter->only_premium);
                $boardFilter['parse_from_date'] = $filter->parse_from_date ? $filter->parse_from_date : '';
            } break;

            case 'otomoto_pl' : {

            } break;
        }

        if (!sizeof($boardFilter))
            $boardFilter = null;

        return $boardFilter;
    }

    /**
     * sendToArchive
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendToArchive($id, Request $request)
    {
        $task = TargetingTask::find($id);

        $jsonData = json_decode( $request->getContent(), true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
        );

        if (!$task) {
            $return['errorCode'] = 1;
            $return['message'] = trans('common.objectNotFound');
        }

        if (!$jsonData || !isset($jsonData['value']) ) {
            $return['errorCode'] = 2;
            $return['message'] = trans('common.variablesNeeded');
        }

        if (!$return['errorCode'] && !in_array($task->state, array( TargetingTask::STATE_TASK_NOT_WORKING, TargetingTask::STATE_TASK_STOPPED_BY_ADMIN ) ) ) {
            $return['errorCode'] = 3;
            $return['message'] = trans('common.operationNotAllowed');
        }

        if (!$return['errorCode']) {
            $task->is_archive = 1;
            $task->save();
        }

        return response()->json($return);

    }

    /**
     * sendToArchiveMultiply
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function sendToArchiveMultiply(Request $request)
    {
        $jsonData = json_decode( $request->getContent(), true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(

        );

        if (!$jsonData || !isset($jsonData['value']) || !isset($jsonData['ids']) || !sizeof($jsonData['ids']) ) {
            $return['errorCode'] = 2;
            $return['message'] = trans('common.variablesNeeded');
        }


        if (!$return['errorCode']) {
            $affected = 0;
            $tasks = TargetingTask::whereIn( 'id', $jsonData['ids'] )->get();
            foreach ($tasks as $task ) {

                if ( !in_array($task->state, array( TargetingTask::STATE_TASK_NOT_WORKING, TargetingTask::STATE_TASK_STOPPED_BY_ADMIN ) ) )
                    continue;

                $task->is_archive = $jsonData['value'];
                $task->save();
                $affected++;
            }

            if (!$affected) {
                $return['errorCode'] = 3;
                $return['message'] = trans('common.operationNotAllowed');
            }
        }



        return response()->json($return);

    }

    /**
     * setState
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function setState($id, Request $request)
    {

        $allowedStates = TargetingTask::allowedStates();
        $task = TargetingTask::find($id);

        $jsonData = json_decode( $request->getContent(), true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
        );

        if (!$task) {
            $return['errorCode'] = 1;
            $return['message'] = trans('common.objectNotFound');
        }

        if (!$jsonData || !isset($jsonData['data']['value']) ) {
            $return['errorCode'] = 2;
            $return['message'] = trans('common.variablesNeeded');
        }

        if (!$return['errorCode'] && $task->source_type &&  ( !isset($allowedStates[$task->state]) || !in_array( $jsonData['data']['value'], $allowedStates[$task->state]))) {
            $return['errorCode'] = 3;
            $return['message'] = trans('common.operationNotAllowed');
        }

        if (!$return['errorCode']) {
            $task->state = $jsonData['data']['value'];
            $task->save();
        }

        return response()->json($return);

    }

    /**
     * setStateMultiply
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function setStateMultiply(Request $request)
    {
        $allowedStates = TargetingTask::allowedStates();
        $jsonData = json_decode( $request->getContent(), true);

        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(

        );

        if (!$jsonData || !isset($jsonData['data']['value']) || !isset($jsonData['data']['ids']) || !sizeof($jsonData['data']['ids']) ) {
            $return['errorCode'] = 2;
            $return['message'] = trans('common.variablesNeeded');
        }

        if (!$return['errorCode']) {
            $affected = 0;
            $tasks = TargetingTask::whereIn( 'id', $jsonData['data']['ids'] )->get();
            foreach ($tasks as $task ) {

                if ( !$task->source_type || !isset($allowedStates[$task->state]) || !in_array( $jsonData['data']['value'], $allowedStates[$task->state]))
                    continue;

                $task->state = $jsonData['data']['value'];
                $task->save();
                $affected++;
            }

            if (!$affected) {
                $return['errorCode'] = 3;
                $return['message'] = trans('common.operationNotAllowed');
            }
        }

        return response()->json($return);

    }


}