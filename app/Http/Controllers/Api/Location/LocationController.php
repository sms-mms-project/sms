<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 22.07.2020
 * Time: 13:16
 */

namespace App\Http\Controllers\Api\Location;

use App\Http\Controllers\Controller,
    Illuminate\Http\Request,
    Illuminate\Http\Response,
    Illuminate\Database\Eloquent\Model,
    App\Models\Location\Country,
    App\Models\Location\Region,
    App\Models\Location\City,
    Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    /**
     * Repository instance
     * @var Model $model
     */
    public $model;


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function countries( Request $request)
    {

        $withRegions = $request->has('withRegions') ? intval($request->get('withRegions') ) : true;
        $withCities = $request->has('withCities') ? intval($request->get('withCities') ) : true;

        $data = Country::when($withRegions, function ($query) {
            return $query->with('regions');
        })->when($withCities, function ($query) {
            return $query->with('regions.cities');
        })
            ->select('id', 'name')
            ->get();


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $data->count()],
            'items' => $data->toArray(),
        );

        return response()->json($return);
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function regions( Request $request)
    {

        $withCities = $request->has('withCities') ? intval($request->get('withCities') ) : true;
        $countryId = $request->has('countryId') ? intval($request->get('countryId') ) : null;

        $data = Region::when($withCities, function ($query) {
            return $query->with('cities');
        })->when($countryId, function ($query) use($countryId) {
            return $query->where('country_id' , '=', $countryId);
        })
            ->select('id', 'name')
            ->get();


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $data->count()],
            'items' => $data->toArray(),
        );

        return response()->json($return);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function cities( Request $request)
    {

        $regionId = $request->has('regionId') ? intval($request->get('regionId') ) : null;

        $data = City::when($regionId, function ($query) use($regionId) {
            return $query->where('region_id' , '=', $regionId);
        })
            ->select('id', 'name', 'region_id')
            ->get();


        $return = array();
        $return['errorCode'] = 0;
        $return['message'] = '';
        $return['data'] = array(
            'stat' => ['itemsCount' => $data->count()],
            'items' => $data->toArray(),
        );

        return response()->json($return);
    }
}