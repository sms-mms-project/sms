<?php

namespace App\Http\Middleware;

use Closure,
    Illuminate\Support\Facades\App,
    Illuminate\Support\Facades\Config,
    Illuminate\Support\Facades\Session,
    Cookie;


class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (isset($_COOKIE['locale']) && array_key_exists($_COOKIE['locale'], Config::get('app.locales'))) {
            App::setLocale($_COOKIE['locale']);
            Session::put('locale', $_COOKIE['locale']);
        } else if (Session::get('locale' ) && array_key_exists( Session::get('locale'), Config::get('app.locales')) ) {
            App::setLocale(Session::get('locale' ));
            //setcookie('locale', $fallbackLocale, time() + 60*60*24*31, '/');
        } else {
            $fallbackLocale = Config::get('app.fallback_locale');
            //setcookie('locale', $fallbackLocale, time() + 60*60*24*31, '/');
            App::setLocale($fallbackLocale);
            Session::put('locale', $fallbackLocale);
        }
        return $next($request);
    }
}
