<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;

class Country extends Model
{
    use RealPagination;

    protected $table = 'countries';

    public $timestamps = true;

    public function regions()
    {
        return $this->hasMany('App\Models\Location\Region', 'country_id')->select('id', 'name', 'country_id');
    }
}
