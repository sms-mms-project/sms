<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;

class City extends Model
{
    use RealPagination;

    protected $table = 'cities';

    public $timestamps = true;

    public function region()
    {
        return $this->belongsTo('App\Models\Location\Region', 'region_id' )->select('id', 'name', 'country_d');
    }
}
