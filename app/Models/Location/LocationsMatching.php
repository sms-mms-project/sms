<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class LocationsMatching extends Model
{
    const TYPE_COUNTRY = 0;
    const TYPE_REGION = 1;
    const TYPE_CITY = 2;

    protected $table = 'locations_matching';

    public static function getData($boardId, $modelId)
    {
        $res = LocationsMatching::where( 'board_id', '=', $boardId )->where('model_id', '=', $modelId)->get();
        return $res ? $res[0]->data  : null;
    }
}
