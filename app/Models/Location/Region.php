<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;

class Region extends Model
{
    use RealPagination;

    protected $table = 'regions';

    public $timestamps = true;

    public function cities()
    {
        return $this->hasMany('App\Models\Location\City', 'region_id')->select('id', 'name', 'region_id');
    }
}
