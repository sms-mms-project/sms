<?php

namespace App\Models\Targeting;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;
use phpDocumentor\Reflection\Types\Static_;

class BoardCategory extends Model
{
    use RealPagination;

    protected $table = 'boards_categories';

    public $timestamps = true;

    public function parent()
    {
        return $this->belongsTo(BoardCategory::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(BoardCategory::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        //$categories = Category::with('childrenRecursive')->whereNull('parent')->get();
        return $this->children()->with('childrenRecursive');
    }
}
