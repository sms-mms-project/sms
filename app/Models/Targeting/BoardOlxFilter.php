<?php

namespace App\Models\Targeting;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;

class BoardOlxFilter extends Model
{
    use RealPagination;

    protected $table = 'board_olx_pl_filters';

    public $timestamps = true;

}
