<?php

namespace App\Models\Targeting;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination;

class TargetAudience extends Model
{
    use RealPagination;

    protected $table = 'target_audiences';

    public $timestamps = true;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = ['name', 'source_type', 'source_id', 'source_file', 'contacts_count', 'board_id', 'created_at', 'updated_at'];
}
