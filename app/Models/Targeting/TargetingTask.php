<?php

namespace App\Models\Targeting;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination,
    App\Models\Targeting\Board,
    App\Models\Targeting\TargetAudience;

class TargetingTask extends Model
{
    use RealPagination;

    const STATE_TASK_NOT_WORKING = 0;       // Не работает
    const STATE_TASK_STARTING = 1;          // Запускается
    const STATE_TASK_WORKING = 2;           // Работает
    const STATE_TASK_STOPPING = 3;          // Останавливается
    const STATE_TASK_FREEZING = 4;          // Замораживается
    const STATE_TASK_FREEZED = 5;           // Заморожен
    const STATE_TASK_UNFREEZE = 6;          // Размораживается
    const STATE_TASK_STOPPING_BY_ADMIN = 7; // Остановливается админом
    const STATE_TASK_STOPPED_BY_ADMIN = 8;  // Остановлен админом

    const TASK_SOURCE_TYPE_FILE  = 0;       // Источник файл
    const TASK_SOURCE_TYPE_BOARD = 1;       // Источник доска обявлений

    protected $table = 'targeting_tasks';

    public $timestamps = true;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'source_type', 'source_id', 'source_file', 'board_id',
        'created_at', 'updated_at', 'pars_start_date', 'pars_finish_date',
        'last_update_date', 'state', 'stop_comment', 'type', 'is_archive', 'target_audience_id'
    ];


    public function board()
    {
        return $this->belongsTo(Board::class, 'board_id');
    }

    public function targetAudience()
    {
        return $this->belongsTo(TargetAudience::class, 'target_audience_id');
    }

    public static function allowedStates()
    {
        $allowedStates = array();
        $allowedStates[TargetingTask::STATE_TASK_NOT_WORKING] = array(TargetingTask::STATE_TASK_STARTING);
        $allowedStates[TargetingTask::STATE_TASK_WORKING] = array(TargetingTask::STATE_TASK_STOPPING, TargetingTask::STATE_TASK_FREEZING);
        $allowedStates[TargetingTask::STATE_TASK_FREEZED] = array(TargetingTask::STATE_TASK_STOPPING, TargetingTask::STATE_TASK_UNFREEZE);

        return $allowedStates;
    }


}
