<?php

namespace App\Models\Targeting;

use Illuminate\Database\Eloquent\Model,
    App\Traits\RealPagination,
    App\Models\Targeting\TargetAudience;

class TargetContact extends Model
{
    use RealPagination;

    protected $table = 'target_contacts';

    public $timestamps = true;

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'phone_str', 'country_id', 'region_id',
        'city_id', 'created_at', 'updated_at',
        'target_audience_id', 'board_id',
    ];

    public function targetAudience()
    {
        return $this->belongsTo(TargetAudience::class, 'target_audience_id');
    }

}
