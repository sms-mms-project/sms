<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 05.09.2020
 * Time: 17:59
 */

namespace App\Classes\Parsers;

use App\Models\Location\City;
use App\Models\Location\Country;
use App\Models\Location\LocationsMatching;
use App\Models\Location\Region;
use App\Models\Settings\Proxies\Proxies;
use App\Models\Targeting\Board;
use App\Models\Targeting\BoardCategory;
use App\Models\Targeting\BoardOlxFilter;
use App\Models\Targeting\TargetingTask;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class OlxPlParser
{
    public const UID_NAME = 'olx_pl';

    public const BASE_URL = 'https://m.olx.pl';

    public $fromSeed = false;

    public  function  __construct($fromSeed = false)
    {
        $this->fromSeed = $fromSeed;
    }

    public static function getUrl($filter, $page = 1, $perPage = 50)
    {
        /*
        $filter['category_id'] = 'integer|min:0';
        $filter['subcategory_id'] = 'integer|min:0';
        $filter['region_id'] = 'integer|min:0';
        $filter['city_id'] = 'integer|min:0';
        $filter['location_distance'] = 'present|integer|min:1';
        $filter['keywords'] = 'string:min:3';
        $filter['price_from'] = 'integer|min:0';
        $filter['price_to'] = 'integer|min:1';
        $filter['offer_type'] = 'integer|between:0,2';
        $filter['is_ready_to_bargain'] = 'integer|between:0,1'; // Если item.params[0].value.arranged == true то цена договорная
        $filter['only_premium'] = 'integer|between:0,1'; // Если item.promotion.highlighted == true то тогда обявление является премиум
        $filter['parse_from_date'] = 'date';
        */

        $boards = Board::where( 'uid_name' , '=', self::UID_NAME )->get();
        $boardId = $boards[0]->id;

        //Создам ссылку для парсинга
        $url  = self::BASE_URL . '/api/v1/offers/?';
        $url .= 'offset=' .( $page - 1) * $perPage . '&';
        $url .= 'limit=' . $perPage . '&';

        $url .= ($filter['category_id'] || $filter['subcategory_id']) ? ($filter['subcategory_id'] ? 'category_id=' . BoardCategory::find($filter['subcategory_id'])->data . '&' : 'category_id=' . BoardCategory::find($filter['category_id'])->data . '&' )  : '';
        $url .= $filter['region_id'] ? 'region_id=' . LocationsMatching::getData($boardId, $filter['region_id'] ) . '&' : '';
        $url .= $filter['city_id'] ? 'city_id=' . LocationsMatching::getData($boardId, $filter['city_id'] ) . '&' : '';
        $url .= $filter['location_distance'] ? 'distance=' . $filter['location_distance'] . '&' : '';
        $url .= $filter['price_from'] > 0 ? 'filter_float_price%3Afrom=' . $filter['price_from'] . '&' : '';
        $url .= $filter['price_to'] > 0 ? 'filter_float_price%3Ato=' . $filter['price_to'] . '&' : '';
        $url .= $filter['offer_type'] > 0 ? ( $filter['offer_type'] == 1 ? 'owner_type=private&' : 'owner_type=business&') : '';

        return array(
            'type'=> 'GET',
            'url' => $url
        );
    }

    public function parseCategories()
    {

        $boards = Board::where( 'uid_name' , '=', self::UID_NAME )->get();
        if (!$boards) {
            if (!$this->fromSeed)
                echo 'Olx board not found in database';
            return false;
        }

        $boardId = $boards[0]->id;

        $client = new Client();
        $response = $client->request('GET', 'https://www.olx.pl');

        $crawler = new Crawler((string)$response->getBody());
        $categories = $crawler->filter('a.parent');

        if (!$categories->count()) {
            if (!$this->fromSeed)
                echo 'Categories not found';
            return false;
        }

        //Удаляем все категории данной доски
        BoardCategory::where('board_id', $boardId)->delete();

        //Создаём все категории
        $categories->each(function(Crawler $node, $i) use($crawler, $boardId){
            $category = array();
            $category['name'] = $node->text();
            $category['id'] = $node->attr('data-id');
            $subCategories = $crawler->filter('[data-category-id="'. $category['id'] . '"]');
            if (!$this->fromSeed)
                echo 'Category ' . $category['name'] . ' ID=' . $category['id'] . "; Sub categories count " . $subCategories->count() . " \n";

            if ($subCategories->count() > 0) {
                $boardCategory = new BoardCategory();
                $boardCategory->name = $category['name'];
                $boardCategory->board_id = $boardId;
                $boardCategory->parent_id = 0;
                $boardCategory->data = $category['id'];
                $boardCategory->save();

                $subCategories->each(function(Crawler $subNode, $i2) use($category, $boardId, $boardCategory) {
                    $subCategory = array();
                    $subCategory['name'] = $subNode->text();
                    $subCategory['id'] = $subNode->attr('data-id');

                    $sboardCategory = new BoardCategory();
                    $sboardCategory->name = $subCategory['name'];
                    $sboardCategory->board_id = $boardId;
                    $sboardCategory->parent_id = $boardCategory->id;
                    $sboardCategory->data = $subCategory['id'];
                    $sboardCategory->save();
                });
            }
        });
    }

    public function parseLocations()
    {

        $boards = Board::where( 'uid_name' , '=', self::UID_NAME )->get();
        if (!$boards) {
            if (!$this->fromSeed)
                echo 'Olx board not found in database';
            return false;
        }

        $boardId = $boards[0]->id;
        $data = \Storage::disk('public')->get('data/olxpl-locations.html');

        LocationsMatching::where('board_id', $boardId)->delete();
        City::truncate();
        Region::truncate();
        Country::truncate();

        $crawler = new Crawler($data);
        $locations = $crawler->filterXPath('//a[contains(@class, "rel block tdnone regionSelectA1")]');

        if (!$locations->count()) {
            if (!$this->fromSeed)
                echo 'Locations not found';
            return false;
        }
        echo $locations->count();

        $country = new Country();
        $country->name = 'Polska';
        $country->save();



        //Создаём все локации
        $locations->each(function(Crawler $node, $i) use ($crawler, $boardId, $country){

            if (($i % 2))
                return ;

            $location = array();
            $location['name'] = $node->text();
            $location['id'] = $node->attr('data-id');

            $region = new Region();
            $region->name = $location['name'];
            $region->country_id = $country->id;
            $region->save();

            $locMatch = new LocationsMatching();
            $locMatch->board_id = $boardId;
            $locMatch->model_id = $region->id;
            $locMatch->type = LocationsMatching::TYPE_REGION;
            $locMatch->data = $location['id'];
            $locMatch->save();


            $subLocations = $crawler->filter('[id="a-region-'. $location['id'] . '"]');
            if (!$this->fromSeed)
                echo 'Category ' . $location['name'] . ' ID=' . $location['id'] . "; Sub categories count " . $subLocations->count() . " \n";

            if ($subLocations->count() > 0) {
                $subLocations->each(function(Crawler $subNode, $i2) use($boardId, $region) {
                    $subLocation = array();
                    $subLocation['name'] = $subNode->text();
                    $subLocation['id'] = $subNode->attr('data-id');
                    $city = new City();
                    $city->name = $subLocation['name'];
                    $city->region_id = $region->id;
                    $city->save();

                    $locMatch = new LocationsMatching();
                    $locMatch->board_id = $boardId;
                    $locMatch->model_id = $city->id;
                    $locMatch->type = LocationsMatching::TYPE_CITY;
                    $locMatch->data = $subLocation['id'];
                    $locMatch->save();
                });
            }
        });
    }

    public function start(TargetingTask $task)
    {
        echo 'Start ' . $task->id . "\n";
        $task->state = TargetingTask::STATE_TASK_WORKING;
        $task->save();
        $taskId = $task->id;
        $page = 0;
        $perPage = 30;
        $client = new Client();
        while(true) {

            $task = TargetingTask::find($taskId);
            if (!$task)
                break;

            //Если останавливается
            if ($task->state == TargetingTask::STATE_TASK_STOPPING) {
                $task->state = TargetingTask::STATE_TASK_NOT_WORKING;
                $task->save();
                break;
            }

            //Если останавливается админом
            if ($task->state == TargetingTask::STATE_TASK_STOPPING_BY_ADMIN) {
                $task->state = TargetingTask::STATE_TASK_STOPPED_BY_ADMIN;
                $task->save();
                break;
            }

            //Если замараживается
            if ($task->state == TargetingTask::STATE_TASK_FREEZING) {
                $task->state = TargetingTask::STATE_TASK_FREEZED;
                $task->save();
            }

            //Если разамараживается
            if ($task->state == TargetingTask::STATE_TASK_UNFREEZE) {
                $task->state = TargetingTask::STATE_TASK_WORKING;
                $task->save();
            }

            //Начинаем процес парсинга
            if ($task->state == TargetingTask::STATE_TASK_WORKING) {
                $this->parse($task, $client, $page, $perPage);
            }

            usleep(1000000);
        }

    }

    public function parse( TargetingTask $task, $client, $page, $perPage)
    {
        $page++;
        $filter = BoardOlxFilter::find( $task->source_id )->toArray();
        $url = self::getUrl( $filter, $page, $perPage );
        $proxy = '';
        while(true) {
            $proxy = $this->getNextProxy($task);
            if (!$proxy)
                return [
                    'errorCode' => 1,
                    'message' => 'Proxy is not found'
                ];
            $response = $client->request( $url['type'], $url['url'], ['proxy' => $proxy] );
            $json = json_decode((string)$response->getBody(),true);

            if ($response->getStatusCode() == 200){
                break;
            }
        }

        if (sizeof($json['data'])) {
            dump( $url, $json );
        }
    }

    public function getNextProxy(TargetingTask $task)
    {
        $proxies = Proxies::where( 'status', 1)->where('is_banned', 0)->where('busy_by_task_id', 0)->get();
        if (!sizeog($proxies))
            return false;
        $index = mt_rand(0, sizeof($proxies)-1 );
        $proxy = $proxies[$index];
        $types = array_flip(Proxies::PROXY_TYPES);
        $proxyStr = $types[$proxy->type] . '://';
        if ($proxy->login || $proxy->password)
            $proxyStr .= $proxy->login . ':' . $proxy->password . '@';
        $proxyStr.= $proxy->ip . ':' . $proxy->port;
        $proxy->busy_by_task_id = $task->id;
        $proxy->save();
        return $proxyStr;
    }
}