<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 12.09.2020
 * Time: 12:07
 */

namespace App\Classes\Demons;


use App\Classes\Process\PersistentProcess;
use App\Classes\Threads\ParserThread;
use App\Models\Targeting\TargetingTask;

class ParserDemon
{

    const USLEEP_INTERVAL = 2000000;

    /**
     * Запуск демона парсера
     *
     * @return null
     */
    public static function start()
    {
        $command = [PHP_BINARY, 'public/parser-demon.php', '&'];
        $command = ['zts-php', 'public/parser-demon.php', '&'];

        $exists = \Storage::disk('local')->exists('pids/parser-demon.pid');
        if ($exists) {
            $pid = \Storage::disk('local')->get('pids/parser-demon.pid');
            if (posix_getpgid($pid)) {
                echo "Server already started\n";
                return true;
            }
        }
        $process = new PersistentProcess($command);
        $process->disableOutput();
        $process->start();
        \Storage::disk('local')->put('pids/parser-demon.pid', $process->getPid());
        echo "Server started\n";
    }

    /**
     * Остановка демона парсера
     *
     * @return null
     */
    public static function stop()
    {
        $exists = \Storage::disk('local')->exists('pids/parser-demon.pid');
        if ($exists) {
            $pid = \Storage::disk('local')->get('pids/parser-demon.pid');
            $out = array();
            exec('kill -9 ' . $pid,$out);
            \Storage::disk('local')->delete('pids/parser-demon.pid');
        }
    }

    /**
     * Запуск логики парсинга
     *
     * @return null
     */
    public static function run()
    {
        static $pool = array();
        $toCheck = [TargetingTask::STATE_TASK_STARTING, TargetingTask::STATE_TASK_WORKING, TargetingTask::STATE_TASK_FREEZED, TargetingTask::STATE_TASK_STOPPING];
        while (true) {
            $tasks = TargetingTask::whereIn( 'state', $toCheck )->orderBy('id', 'desc')->get();
            if (sizeof($tasks))
                foreach ($tasks as $task) {

                    // Запускаемые и работающие
                    if ( in_array($task->state, [TargetingTask::STATE_TASK_STARTING, TargetingTask::STATE_TASK_WORKING]) ) {
                        if ( !isset($pool[$task->id]) ) {
                            $thread = new ParserThread($task);
                            $thread->run();
                            $pool[$task->id] = $task;
                        }  elseif ( $pool[$task->id]->isTerminated() ) {
                            unset($pool[$task->id]);
                        }
                    }

                    // Замароженные Остонавливаемые
                    if ( in_array($task->state, [TargetingTask::STATE_TASK_FREEZED, TargetingTask::STATE_TASK_STOPPING]) ) {
                        if ( !isset($pool[$task->id]) ) {
                            $task->state = TargetingTask::STATE_TASK_NOT_WORKING;
                            $task->save();
                        }  elseif ( $pool[$task->id]->isTerminated() ) {
                            unset($pool[$task->id]);
                            $task->state = TargetingTask::STATE_TASK_NOT_WORKING;
                            $task->save();
                        }
                    }

                    //self::runTask($task);
                }

            usleep(self::USLEEP_INTERVAL );
        }
    }

    /**
     * Запуск сообствено парсера
     *
     * @return null
     */
    public static function runTask($task)
    {
    }
}