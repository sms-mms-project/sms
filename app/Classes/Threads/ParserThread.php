<?php
/**
 * Created by PhpStorm.
 * User: shera
 * Date: 12.09.2020
 * Time: 16:34
 */

namespace App\Classes\Threads;

use App\Classes\Parsers\OlxPlParser;
use App\Models\Targeting\TargetingTask;
use App\Models\Targeting\Board;

class ParserThread extends \Thread
{
    public $task = null;
    public $parser = null;

    public function __construct(TargetingTask $task)
    {
        $this->task = $task;

        $board = Board::find($task->board_id);
        switch ($board->uid_name)
        {
            case 'olx_pl': {
                $this->parser = new OlxPlParser();
            } break;
            case 'otomoto_pl': {
                echo $board->uid_name . "\n";
            } break;
        }
    }

    public function run()
    {
        if ($this->parser)
            $this->parser->start($this->task);
    }
}